from django import forms
from .models.profiles import ProfileData


class CompanyForm(forms.ModelForm):
    class Meta:
        model = ProfileData 
        fields = ("company","activity", "phone", "mobile_phone",
                  "company_members","locations")

