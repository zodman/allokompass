#!/usr/bin/env python
# encoding=utf-8
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
# made by zodman

from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from account.models import Account
from django.db import models


class ProfileData(models.Model):
    user = models.OneToOneField(User)
    company = models.CharField(max_length=100,  help_text=u"Nombre de la compañia")
    activity = models.CharField(max_length=100,)
    phone = models.DecimalField(max_digits=20,  decimal_places=0)
    mobile_phone= models.DecimalField(max_digits=20, decimal_places=0)
    company_members = models.PositiveIntegerField(default=0)
    locations = models.CharField(max_length=100, help_text=u"Ubicación de la compañia")

    def __unicode__(self):
        return self.company

