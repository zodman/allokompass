from django.db import models
from django.conf import settings

User = settings.AUTH_USER_MODEL

# {{{ sections
class Section(models.Model):
    name = models.CharField(max_length=100)
    order = models.PositiveIntegerField()
    
    class Meta:
        ordering = ("order",)

    def __unicode__(self):
        return u"{}".format(self.name)

class SubSection(models.Model):
    name = models.CharField(max_length=100)
    section = models.ForeignKey("Section", related_name="subsections")
    order = models.PositiveIntegerField()
    
    class Meta:
        ordering = ("order",)


    def __unicode__(self):
        return u"[{}] {}".format(self.section.name, self.name)

    def had_unanswer_questions(self):
        qs  = self.questions.all()
        for i in qs:
            if not i.had_answer():
                return False
        return True

# }}}

class PosibleAnswer(models.Model):
    name = models.CharField(max_length=100)
    value = models.PositiveIntegerField()
    
    class Meta:
        ordering = ("value",)


    def __unicode__(self):
        return u"{}".format(self.name)

class GroupPosibleAnswer(models.Model):
    description = models.CharField(max_length=100)
    posible_answer = models.ManyToManyField("PosibleAnswer")

    def __unicode__(self):
        return self.description

class Question(models.Model):
    subsection = models.ForeignKey(SubSection, related_name="questions")
    ask = models.TextField()
    posible_answer = models.ForeignKey(GroupPosibleAnswer)
    recomendation = models.TextField(null=True, blank=True)


    def __unicode__(self):
        return u"{}".format(self.ask)

    def had_answer(self):
        return self.answers.exists()

# {{{ answer
class Answer(models.Model):
    answer = models.ForeignKey(PosibleAnswer)
    question = models.ForeignKey(Question, related_name="answers")
    user = models.ForeignKey(User) 

    class Meta:
        unique_together = ("answer", "question", "user")

    def __unicode__(self):
        return u"{}".format(self.id)
#}}}
