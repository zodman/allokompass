from django.contrib import admin
from .models import *

class QuestionAdmin(admin.ModelAdmin):
    list_display = ("ask", "get_subsection", "section")

    def get_subsection(self, obj):
    	return obj.subsection.name
    get_subsection.admin_order_field = "subsection"

    def section(self, obj):
        return obj.subsection.section.name
    section.admin_order_field = "subsection__section"

class AnswerAdmin(admin.ModelAdmin):
    list_display = ("__unicode__", "user", "question")

class SubSectionAdmin(admin.ModelAdmin):
    list_display = ("name", "section", 'order')
    list_editable=("order",)
class SectionAdmin(admin.ModelAdmin):
    list_display = ("name", "order")
    list_editable=("order",)

class PosibleAdmin(admin.ModelAdmin):
    list_display =  ("name", "value", "get_group")

    def get_group(self, obj):
        all = obj.groupposibleanswer_set.all()
        if all:
            return list(all).pop()
            


admin.site.register(Section, SectionAdmin)
admin.site.register(SubSection, SubSectionAdmin)
admin.site.register(GroupPosibleAnswer)
admin.site.register(PosibleAnswer, PosibleAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)

admin.site.register(ProfileData)
