from django import template
register = template.Library()

@register.filter
def unanswer(questions_input, user_args):
    questions = questions_input.exclude(answers__user=user_args)
    return questions


@register.filter
def calif(questions_input, user_args):
    answer = questions_input.answers.get(user__id= user_args.id)
    return answer.answer.value >= 4
