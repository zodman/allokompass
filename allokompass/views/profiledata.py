from django.views.generic import UpdateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from allokompass.models import ProfileData

class CreateProfileData(LoginRequiredMixin, CreateView):
    model = ProfileData
    fields = ("company", "activity", "phone",
            "mobile_phone", "company_members",
            "locations")



    def form_valid(self, form):
        profiledata = form.save(commit=False)
        profiledata.user = self.request.user
        profiledata.save()


create_profiledata = CreateProfileData.as_view()

class UpdateProfileData(UpdateView):
    model = ProfileData

update_profiledata = UpdateProfileData.as_view()    

