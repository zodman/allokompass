from django.views.generic import TemplateView, FormView
from allokompass.forms import CompanyForm
from allokompass.models import ProfileData, PosibleAnswer
from allokompass.models import Question, Section, Answer
from allokompass.templatetags.questions_tags import unanswer

class QuestionView(TemplateView):
    template_name="allokompass/questions.html"
    form_class = CompanyForm


    def process_post(self, post, user ):
        for k in post.keys():
            if "q_" in k:
                question_id = int(k.replace("q_",""))
                question = Question.objects.get(id=question_id)
                answer_id = post.get(k)
                posible_answer = PosibleAnswer.objects.get(id=answer_id)
                exists = Answer.objects.filter(user=user, question=question, answer=posible_answer).exists()
                if not exists:
                    Answer.objects.create(user=user, question=question, answer=posible_answer)



    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        self.process_post(request.POST, request.user)
        return self.render_to_response(context)

    def check_user_answer_all_questions(self, user):
        qs = Question.objects.all()
        qs = unanswer(qs, user)
        return not qs.exists()

    def get_context_data(self, **context):
        sections = Section.objects.all()
        user_answer_all = self.check_user_answer_all_questions(self.request.user)
        context.update({'user_answer_all': user_answer_all})
        try:
            self.request.user.profiledata
            context.update({'sections':sections})
            return context
        except ProfileData.DoesNotExist:
            pass
        if self.request.POST:
            companyform = CompanyForm(self.request.POST)
            if companyform.is_valid():
                data  = companyform.save(commit = False)
                data.user = self.request.user
                data.save()
                companyform = None

        else:
            companyform = CompanyForm()
        context.update({
            'company_form':companyform,
            'sections':sections
        })
        return context

question = QuestionView.as_view()


